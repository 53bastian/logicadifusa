using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    public Cat cat;
    public int fishCount=0;
    public bool doingAction = false;
    public Text fishCounTxt;
    public Image barra;

    // Start is called before the first frame update
    void Start()
    {
        fishCounTxt.text = "x" + fishCount;
        barra.fillAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (cat.hungry<2)
        {
            if (fishCount <10)
            {
                Fish();
            }
        }
        else if(cat.hungry<3)
        {
            if (cat.fishCount+fishCount <10)
            {
                Fish();
            }
            else
            {
                FeedCat();
            }
        }
        else 
        {
            if (fishCount > 0)
            {
                FeedCat();
            }
            else
            {
                Fish();
            }
        }
    }
    public void FeedCat()
    {
        if (!doingAction)
        {
            StartCoroutine(TryToFeed());
        }
    }
    public void Fish()
    {
        if (!doingAction)
        {
            StartCoroutine(TryToFish());
        }
    }

    public IEnumerator TryToFish()
    {
        doingAction = true;
        for (int i = 0; i < 10; i++)
        {
            barra.fillAmount += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        if (Random.Range(0,10)>=2)
        {
            fishCount++;
        }
        fishCounTxt.text = "x" + fishCount;
        barra.fillAmount = 0;
        doingAction = false;
    }
    public IEnumerator TryToFeed()
    {
        doingAction = true;
        for (int i = 0; i < 10; i++)
        {
            barra.fillAmount += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        fishCount--;
        cat.Feed();
        fishCounTxt.text = "x" + fishCount;
        barra.fillAmount = 0;
        doingAction = false;
    }
}
