using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cat : MonoBehaviour
{
    public float hungry=0;
    public float speed = 1;
    public int fishCount=0;
    public Text fishCounTxt;
    public Image barra;
    
    // Start is called before the first frame update
    void Start()
    {
        fishCounTxt.text = "x"+fishCount;
    }

    // Update is called once per frame
    void Update()
    {
        if (hungry < 5)
        {
            hungry += Time.deltaTime*speed;
            barra.fillAmount = (5.0f-hungry) / 5.0f;
        }    
    }

    public void Feed()
    {
        fishCount++;
        if (fishCount==10)
        {
            hungry = 0;
            fishCount = 0;
        }
        fishCounTxt.text = "x" + fishCount;
    }
}
